from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.safestring import mark_safe

import json

# Create your views here.
@login_required
def index(request):
  return render(request, 'imifield/index.html', {})

@login_required
def room(request, room_id):
  return render(request, 'imifield/room.html', {
    'room_id': room_id,
    'room_id_json': mark_safe(json.dumps(room_id)),
  })
