from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
  re_path(r'ws/imifield/(?P<room_id>\w+)/$', consumers.ImifieldConsumer.as_asgi()),
]