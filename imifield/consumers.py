from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from .game import Game
import json


class ImifieldConsumer(WebsocketConsumer):
    def connect(self):
        self.room_id = self.scope["url_route"]["kwargs"]["room_id"]
        self.room_group_name = "imifield_%s" % self.room_id
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name, self.channel_name
        )
        self.accept()
        setattr(ImifieldConsumer, self.scope["user"].username, self.channel_name)
        self.game = Game(self, self.room_id)
        self.room_enter(self.room_id)

    def disconnect(self, code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name, self.channel_name
        )
        self.room_exit(self.room_id)

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        async_to_sync(self.channel_layer.send)(
            self.channel_name, {"type": text_data_json["type"], **text_data_json}
        )

    def __getattr__(self, name):
        # message type がこのリストにあれば
        # message をそのまま WebSocket に流すハンドラを返す
        passthrough = ["sys_msg", "ready", "finish"]

        def echo(event):
            self.send(text_data=json.dumps(event))

        if name in passthrough:
            return echo

    def damage(self, event):
        user = self.scope["user"]
        damage_val = event.get(user.username)

        if damage_val:
            hp_remaining = self.game.player_damage(user.username, damage_val)
            if hp_remaining == 0:
                self.room_message(
                    f"{self.game.get_display_name(user.username)}さんは力尽きました。"
                )
                self.send(text_data='{"type":"ready","ready":false}')
                self.room_exit(passive=True)
            self.send(
                text_data=f'{{"type":"damage","damage":{damage_val}, "hp":{hp_remaining}}}'
            )

    def dice(self, event):
        user = self.scope["user"]
        dice_num = self.game.player_dice(user.username)
        self.send(text_data='{"type":"ready","ready":false}')
        self.send(text_data=f'{{"type":"dice","number":{dice_num}}}')
        self.room_message(
            f"{self.game.get_display_name(user.username)}さんはダイスを振って、{dice_num}が出ました。"
        )

        game_status = self.game.status
        # プレイヤー全員がダイスを振った
        all_ready = self.game.all_ready

        # 先攻後攻の決定
        if game_status == b"turn_order":
            if all_ready:
                dice_order = self.game.player_order()
                order_str = " > ".join(
                    [f"[{self.game.get_display_name(p)}]" for p in dice_order]
                )
                self.room_message(f"順番が決まりました: \n{order_str}\n")
                self.room_message("=== ゲームスタート ===")
                # いったん全員がダイス振りできないようにする
                self.room_message({"type": "ready", "ready": False}, system=False)
                # 一番目のプレイヤーがダイス振りできるようにする
                self.room_message(
                    {"type": "ready", "ready": True},
                    channel_name=getattr(ImifieldConsumer, dice_order[0]),
                    system=False,
                    broadcast=False,
                )

        if game_status == b"in_turn":
            if not all_ready:
                next_player = self.game.player_next(user.username)
                if next_player:
                    self.room_message(
                        {"type": "ready", "ready": True},
                        channel_name=getattr(ImifieldConsumer, next_player),
                        system=False,
                        broadcast=False,
                    )
            else:
                damages = self.game.damage_calculate()
                self.room_message({"type": "damage", **damages}, system=False)
                # いったん全員がダイス振りできないようにする
                self.room_message({"type": "ready", "ready": False}, system=False)
                # 一番目のプレイヤーがダイス振りできるようにする
                self.room_message(
                    {"type": "ready", "ready": True},
                    channel_name=getattr(ImifieldConsumer, self.game.player_next()),
                    system=False,
                    broadcast=False,
                )

    def player_attack(self, event):
        attacker, defender = self.game.get_display_name(
            event["attacker"]
        ), self.game.get_display_name(event["defender"])
        damage = event["damage"]
        self.room_message(f"{attacker}さんは{defender}さんに{damage}のダメージを与えました")

    def room_message(self, message, channel_name=None, system=True, broadcast=True):
        channel = self.channel_layer
        async_to_sync(channel.group_send if broadcast else channel.send)(
            self.room_group_name if broadcast else (channel_name or self.channel_name),
            {"type": "sys_msg", "msg": message} if system else message,
        )

    def room_enter(self, room_id):
        user = self.scope["user"]
        room_exists = self.game.room_ensure()
        if not room_exists:
            self.room_message(f"ルーム {room_id} が作成されました。プレイヤーを待っています…")

        # 部屋ごとのプレイヤーリストに追加
        self.game.player_add(user.username)
        self.room_message(f"{self.game.get_display_name(user.username)}さんが入室しました。")

        # 先攻後攻決める途中であれば、ダイス振りを促す
        if room_exists and self.game.status == b"turn_order":
            # self.send を使うと sys_msg より先に送られてしまう
            self.room_message("ゲームを開始するには、プレイヤー全員がダイスを振る必要があります。", broadcast=False)
            self.room_message({"type": "ready", "ready": True}, system=False)

    def room_exit(self, passive=False):
      user = self.scope["user"]

      # プレイヤーリストから削除
      self.game.player_remove(user.username)

      if not passive:
          self.room_message(f"{self.game.get_display_name(user.username)}さんが退室しました。")

      last_survivor = self.game.last_survivor
      if last_survivor:
        # ゲーム進行中、ルームに一人しか残っていない
        # ゲームを強制終了
        self.room_message(
            {
                "type": "finish",
                "winner": f"{self.game.get_display_name(last_survivor)}",
            },
            system=False,
        )
        self.game.room_clear()
