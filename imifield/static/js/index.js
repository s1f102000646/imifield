
document.querySelector("#room-id-input").focus();

document.querySelector("#room-id-input").onkeyup = function(e) {
  if (e.keyCode == 13) { // enter
    document.querySelector("#room-id-submit").click();
  }
};

document.querySelector("#room-id-submit").onclick = function(e) {
  const roomId = document.querySelector("#room-id-input").value;
  if (roomId.length != 6) {
    alert("正しいIDを入力してください");
  } else {
    window.location.pathname = "/imifield/" + roomId + "/";
  }
};