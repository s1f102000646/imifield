const elem_room_id = document.querySelector("#room-id");
const elem_dice_button = document.querySelector("#dice-roll");
const elem_log = document.querySelector("#dice-log");

elem_room_id.textContent = room_id;

let endpoint = `${window.location.protocol.replace('http', 'ws')}//${window.location.host}/ws/imifield/${room_id_json}/`;
const roomSocket = new WebSocket(endpoint);

roomSocket.onmessage = function(e) {
  const data = JSON.parse(e.data);
  const type = data["type"];

  if (type == "ready") {
    const ready = data["ready"];
    if (elem_dice_button.disabled && ready) {
      elem_log.value += "ダイスを振ってください。\n";
    }
    elem_dice_button.disabled = !ready;
  } else if (type == "damage") {
    const damage = data["damage"];
    const hp = data["hp"];
    elem_log.value += display_name + "は" + damage + "のダメージを受けた！\n";
    elem_log.value += display_name + "の残りHPは" + hp +"。\n"
  } else if (type == "dice") {
    const number = data["number"];
    elem_log.value += "ダイスロール: " + number + "\n";
  } else if (type == "sys_msg") {
    const msg = data["msg"];
    elem_log.value += "システムログ: " + msg + "\n";
  } else if (type == "finish") {
    const winner = data["winner"];
    elem_dice_button.disabled = true;
    elem_log.value += "ゲーム終了！\n";
    elem_log.value += "勝者：" + winner + "\n";
    elem_log.value += "もう一度プレイするには、リロードしてください。";
    roomSocket.close();
  }
  elem_log.scrollTop = elem_log.scrollHeight;
}

elem_dice_button.onclick = function(e) {
  const req = {"type": "dice"};
  roomSocket.send(JSON.stringify(req));
};