from typing import Dict, List
from asgiref.sync import async_to_sync as wait
from channels.generic.websocket import WebsocketConsumer
from channels_redis.core import ConnectionPool
from django.contrib.auth.models import User as DjangoUser
from itertools import permutations
import json, random

# HP上限
HEALTH_FULL = 10


class ConnectionContextManager:
    def __init__(self, pool: ConnectionPool):
        self.pool = pool

    def __enter__(self):
        self.conn = wait(self.pool.pop)()
        return self.conn

    def __exit__(self, exc_type, exc, tb):
        if exc:
            wait(self.pool.conn_error)(self.conn)
        else:
            self.pool.push(self.conn)
        self.conn = None


class Game:
    def __init__(self, websocket: WebsocketConsumer, room_id: str) -> None:
        self.ws = websocket
        self.room_id = room_id
        self.game_room_prefix = f"game:{self.ws.room_group_name}"
        self.game_status_key = f"{self.game_room_prefix}:status"
        self.players_key = f"{self.game_room_prefix}:players"
        self.ready_players_key = f"{self.players_key}:ready"
        self.dice_key = f"{self.players_key}:dice"
        self.order_key = f"{self.players_key}:order"

    def __channel_message(self, event):
        wait(self.ws.channel_layer.send)(self.ws.channel_name, event)

    def __redis_connection(self) -> ConnectionContextManager:
        return ConnectionContextManager(self.ws.channel_layer.pools[0])

    @property
    def status(self):
        with self.__redis_connection() as conn:
            return wait(conn.get)(self.game_status_key)

    @status.setter
    def status(self, status):
        with self.__redis_connection() as conn:
            return wait(conn.set)(self.game_status_key, status)

    @property
    def all_ready(self):
        with self.__redis_connection() as conn:
            return wait(conn.sdiff)(self.players_key, self.ready_players_key) == []

    @property
    def last_survivor(self):
        with self.__redis_connection() as conn:
            if (
                wait(conn.get)(self.game_status_key) == b"in_turn"
                and wait(conn.scard)(self.players_key) == 1
            ):
                return wait(conn.smembers)(self.players_key)[0].decode()
            else:
                return None

    def get_display_name(self, username):
        with self.__redis_connection() as conn:
            display_name_key = f"user:{username}:display_name"
            display_name = wait(conn.get)(display_name_key)

            if display_name:
                display_name = display_name.decode()
            else:
                user = DjangoUser.objects.get(username=username)
                display_name = f"{user.first_name} {user.last_name}"
                wait(conn.set)(display_name_key, display_name)

            return display_name

    def room_ensure(self):
        with self.__redis_connection() as conn:
            game_status = wait(conn.get)(self.game_status_key)
            if not game_status:
                # ゲーム開始前
                self.status = b"init"
                return False
            else:
                return True

    def room_clear(self):
        with self.__redis_connection() as conn:
            all_game_keys = wait(conn.keys)(f"{self.game_room_prefix}:*")
            for k in all_game_keys:
                wait(conn.delete)(k)

    def player_add(self, username: str):
        with self.__redis_connection() as conn:
            game_status = wait(conn.get)(self.game_status_key)
            if game_status != b"in_turn":
                # ゲームの進行状況: 「先攻後攻の決定」に変更
                self.status = b"turn_order"
            wait(conn.sadd)(self.players_key, username)

    def player_remove(self, username: str):
        with self.__redis_connection() as conn:
            wait(conn.lrem)(self.order_key, 0, username)
            wait(conn.srem)(self.ready_players_key, username)
            wait(conn.srem)(self.players_key, username)

    def player_dice(self, username: str) -> int:
        with self.__redis_connection() as conn:
            wait(conn.sadd)(self.ready_players_key, username)
            random.seed()
            dice_num = random.randint(1, 6)
            wait(conn.zadd)(self.dice_key, dice_num, username)
            return dice_num

    def player_order(self) -> List[str]:
        with self.__redis_connection() as conn:
            dice_order = [
                pair[0].decode()
                for pair in wait(conn.zrevrangebyscore)(
                    self.dice_key, 6, 0, withscores=True
                )
            ]
            wait(conn.delete)(self.order_key)
            wait(conn.rpush)(self.order_key, *dice_order)
            wait(conn.delete)(self.ready_players_key)
            wait(conn.delete)(self.dice_key)
            self.status = b"in_turn"
            return dice_order

    def player_damage(self, username: str, value: int) -> int:
        damage_key = f"{self.game_room_prefix}:{username}:damage"

        with self.__redis_connection() as conn:
            damage_total = wait(conn.incrby)(damage_key, value)
            if damage_total >= HEALTH_FULL:
                damage_total = wait(conn.set)(damage_key, HEALTH_FULL)
                return 0
            return HEALTH_FULL - damage_total

    def player_next(self, username=None) -> str:
        with self.__redis_connection() as conn:
            players = [p.decode() for p in wait(conn.lrange)(self.order_key, 0, -1)]
            if not username:
                return players[0]
            if username in players:
                next_idx = players.index(username) + 1
                # 次のプレイヤーがダイス振りできるようにする
                return players[next_idx] if next_idx < len(players) else None

    def damage_calculate(self) -> Dict[str, int]:
        with self.__redis_connection() as conn:
            players = [p.decode() for p in wait(conn.lrange)(self.order_key, 0, -1)]
            dice_nums = {
                p[0].decode(): p[1]
                for p in wait(conn.zrange)(self.dice_key, withscores=True)
            }
            damages = {p: 0 for p in players}
            for attacker, defender in permutations(players, r=2):
                if dice_nums[attacker] > dice_nums[defender]:
                    damages[defender] += dice_nums[attacker] - dice_nums[defender]
                    self.__channel_message(
                        {
                            "type": "player.attack",
                            "attacker": attacker,
                            "defender": defender,
                            "damage": (dice_nums[attacker] - dice_nums[defender]),
                        }
                    )
            wait(conn.delete)(self.ready_players_key)
            wait(conn.delete)(self.dice_key)
            return damages
